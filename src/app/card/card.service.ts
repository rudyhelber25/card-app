import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CardService {

  private listDate: string[];
  private listContent: string[];

  constructor() {
    this.listDate = ['12 August', '13 june', '22 july', '12 August', '22 october',
                     '19 march', '13 june', '22 july', '2 january', '5 december'];
    this.listContent = ['These docs assume that you are already familiar with HTML, CSS, JavaScript',
                        'These docs assume that you are already familiar with HTML, CSS, JavaScript',
                        'These docs assume that you are already familiar with HTML, CSS, JavaScript',
                        'These docs assume that you are already familiar with HTML, CSS, JavaScript',
                        'These docs assume that you are already familiar with HTML, CSS, JavaScript',
                        'These docs assume that you are already familiar with HTML, CSS, JavaScript',
                        'These docs assume that you are already familiar with HTML, CSS, JavaScript',
                        'These docs assume that you are already familiar with HTML, CSS, JavaScript',
                        'These docs assume that you are already familiar with HTML, CSS, JavaScript',
                        'These docs assume that you are already familiar with HTML, CSS, JavaScript'];
  }


  public get getListDate(): string[] {
    return this.listDate;
  }

  public get getListContent(): string[] {
    return this.listContent;
  }
}
