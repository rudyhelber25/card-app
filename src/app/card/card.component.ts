import { Component, OnInit } from '@angular/core';
import {CardService} from './card.service';

@Component({
  selector: 'todo-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  public listDates: string[];
  public listContents: string[];

  constructor(public cardService: CardService) {
    this.listDates = [];
    this.listContents = [];
  }

  ngOnInit() {
    this.listDates = this.cardService.getListDate;
    this.listContents = this.cardService.getListContent;
  }

}
