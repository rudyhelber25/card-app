import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'todo-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.scss']
})
export class BodyComponent implements OnInit {

  @Input() public content: string;

  constructor() {
  this.content = 'These docs assume that you are already familiar with HTML, CSS, JavaScript, ' +
                 'and some of the tools from the latest standards, such as classes and modules. ' +
                 'The code samples are written using TypeScript. ' +
                 'Most Angular code can be written with just the latest JavaScript, ' +
                 'using types for dependency injection, and using decorators for metadata.';
  }
  ngOnInit() {
  }

}
