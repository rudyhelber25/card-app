import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'todo-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  @Input() public date: string;

  constructor() {
    this.date = '';
  }

  ngOnInit() {
  }

}
