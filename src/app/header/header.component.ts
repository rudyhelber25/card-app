import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'todo-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @Input() public id: number;

  constructor() {
    this.id = 0;
  }

  ngOnInit() {
  }

}
